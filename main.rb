#! /usr/bin/ruby

#require 'nokogiri'
#require 'open-uri'

#doc = Nokogiri::HTML(open('http://www.sverigetopplistan.se/'))
#puts(doc.xpath("//table"))

require 'watir'
require 'headless'
require 'nokogiri'
require 'net/smtp'
require 'inifile'

config = IniFile.load('config.ini')

headless = Headless.new
headless.start
browser = Watir::Browser.new :firefox
browser.goto('http://www.sverigetopplistan.se/')

browser.table(class: 'toppos').wait_until_present

page = Nokogiri::HTML.parse(browser.html)

#Close the browser, no longer needed
browser.close
headless.destroy

artist = page.at_css('.toppos .artist').content
title = page.at_css('.toppos .title').content

message = <<MESSAGE
Subject: Sverigetopplistan

Artist:\t#{artist}
Song:\t#{title}
MESSAGE

smtp_conf = config['Mail']
smtp = Net::SMTP.new(smtp_conf['smtp'], smtp_conf['port'])
smtp.enable_starttls
smtp.start( smtp_conf['domain'],
            smtp_conf['auth_name'],
            smtp_conf['password'],
            smtp_conf['authtype'].to_sym
) do
    smtp.send_message(message,smtp_conf['from'],smtp_conf['to'])
end
